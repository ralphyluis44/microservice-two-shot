import React, { useState, useEffect } from 'react';
import { cardStyle, cardContainerStyle, imageStyle, detailsStyle, buttonStyle, pageContainerStyle} from './ShoeListStyles';
function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const getShoesData = async () => {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    };

    useEffect(() => {
        getShoesData();
    }, []);

    const handleDelete = (id) => {
        fetch ("http://localhost:8080/api/shoes/"+id, { method: "DELETE" })
            .then(() => {
                window.location.reload()
            });
    }
    return (
        <div style={cardContainerStyle}>
            {shoes.map(shoe => (
                <div style={cardStyle} key={shoe.id}>
                    <img
                        src={shoe.picture_url}
                        alt={shoe.model_name}
                        style={imageStyle}
                    />
                    <div style={detailsStyle}>
                        <h3>{shoe.model_name}</h3>
                        <p>{shoe.manufacturer}</p>
                        <p>{shoe.color}</p>
                        <p>{shoe.bin.closet_name}</p>
                        <button
                            onClick={() => handleDelete(shoe.id)}
                            style={buttonStyle}
                        >
                            Delete
                        </button>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default ShoesList;