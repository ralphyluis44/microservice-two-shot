import React, { useState, useEffect } from 'react';

function HatForm() {
  const [style_name, setStyle] = useState('');
  const [fabric, setFabric] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    const fetchLocations = async () => {
      const url = 'http://localhost:8100/api/locations/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    };
    fetchLocations();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      style_name,
      fabric,
      color,
      picture_url: pictureUrl, // Corrected to use pictureUrl state variable
      location,
    };

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      setStyle('');
      setFabric('');
      setColor('');
      setPictureUrl('');
      setLocation('');
    }
  };

  const containerStyle = { color: '#003366' };
  return (
    <div className="container" style={containerStyle}>
      <div className="row">
        <div className="offset-md-3 col-md-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input
                  value={style_name}
                  onChange={(e) => setStyle(e.target.value)}
                  placeholder="Style"
                  required
                  type="text"
                  name="style_name"
                  id="style_name"
                  className="form-control" />
                <label htmlFor="style_name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={color}
                  onChange={(e) => setColor(e.target.value)}
                  placeholder="Color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={fabric}
                  onChange={(e) => setFabric(e.target.value)}
                  placeholder="Fabric"
                  required
                  type="text"
                  name="fabric"
                  id="fabric"
                  className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={pictureUrl}
                  onChange={(e) => setPictureUrl(e.target.value)}
                  placeholder="Picture URL"
                  required
                  type="url"
                  name="picture_url"
                  id="picture_url"
                  className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
                {pictureUrl && (
                  <img src={pictureUrl} alt="Hat Preview" style={{ maxWidth: '100%', marginTop: '10px' }} />
                )}
              </div>
              <div className="form-floating mb-3">
                <select
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                  required
                  name="location_id"
                  id="location_id"
                  className="form-select">
                  <option value="">Select a location</option>
                  {locations.map((location) => (
                    <option key={location.id} value={location.href}>
                      {location.closet_name}
                    </option>
                  ))}
                </select>
                <label htmlFor="location_id">Location Name</label>
              </div>
              <button className="btn btn-primary" type="submit">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
