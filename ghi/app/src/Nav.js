import { NavLink } from 'react-router-dom';

function Nav() {
  const linkStyle = { color: '#003366' };
  const navbarStyle = { marginBottom: 0 };
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-warning"  style={navbarStyle}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/" style={linkStyle}>Wardobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/" style={linkStyle}>Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes" style={linkStyle}>Shoes</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes/list" style={linkStyle}>Shoes List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats" style={linkStyle}>Hats</NavLink>
              </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats/list" style={linkStyle}>HatsList</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
