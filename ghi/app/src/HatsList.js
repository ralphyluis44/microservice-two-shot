import React, { useState, useEffect } from 'react';
import { cardStyle, cardContainerStyle, imageStyle, detailsStyle, buttonStyle   } from './ShoeListStyles';

function HatsList() {
    const [hats, setHats] = useState([]);

    const getHatsData = async () => {
        const response = await fetch("http://localhost:8090/api/hats/");
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
            console.log({data})
        }
    };

    useEffect(() => {
        getHatsData();
    }, []);

    const handleDelete = (id) => {
        fetch ("http://localhost:8090/api/hats/"+id, { method: "DELETE" })
            .then(() => {
                window.location.reload()
            });
    };

    return (
        <div style={cardContainerStyle}>
            {hats.map(hat => (
                <div style={cardStyle} key={hat.id}>
                    <img
                        src={hat.picture_url}
                        alt={hat.style}
                        style={imageStyle}
                    />
                    <div style={detailsStyle}>
                        <h3>{hat.style_name}</h3>
                        <p>{hat.fabric}</p>
                        <p>{hat.color}</p>
                        <p>{hat.location.closet_name}</p>
                        <button
                            onClick={() => handleDelete(hat.id)}
                            style={buttonStyle}
                        >
                            Delete
                        </button>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default HatsList;