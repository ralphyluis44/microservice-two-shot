import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm';
import ShoesList from './ShoeList';
import HatsList from './HatsList';
import HatsForm from './HatsForm';

const rootStyle = {
  height: '100%',
  minHeight: '100vh',
  background: 'linear-gradient(to bottom, #FFFFFF, #ffcc00)',
  color: 'white',
  margin: 0,
  padding: 0,
};
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div style={rootStyle}>

      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeForm />} />
          <Route path="/shoes/list" element={<ShoesList />} />
          <Route path="/hats" element={<HatsForm />} />
          <Route path="/hats/list" element={<HatsList />} />


        </Routes>
      </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
