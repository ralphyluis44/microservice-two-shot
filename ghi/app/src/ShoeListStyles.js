export const cardContainerStyle = {
    display: 'flex',
    flexWrap: 'wrap',
    gap: '20px',
    justifyContent: 'center',
    marginTop: '20px'
};

export const cardStyle = {
    width: '200px',
    borderRadius: '8px',
    overflow: 'hidden',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
    transition: 'box-shadow 0.3s ease',
    margin: '0 10px 20px',
    background: 'linear-gradient(to bottom, #00008B, #FFD700)'
};

export const imageStyle = {
    width: '100%',
    height: '40%'
};

export const detailsStyle = {
    padding: '16px',
    textAlign: 'center',
    color: '#003366'
};

export const buttonStyle = {
    backgroundColor: '#003366',
    color: 'white',
    border: 'none',
    padding: '8px 16px',
    margin: '8px 0',
    cursor: 'pointer',
    borderRadius: '4px',
    width: '100%'
};