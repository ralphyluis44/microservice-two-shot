import React, { useState, useEffect } from 'react';

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const fetchBins = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        }
      };
      useEffect(() => {
        fetchBins();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            manufacturer,
            model_name,
            color,
            picture_url,
            bin
        };

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setColor('');
            setPicture('');
            setBin('');

        }

    };
    const containerStyle = { color: '#003366' };
    return (
        <div className="container" style={containerStyle}>
            <div className="row">
                <div className="offset-md-3 col-md-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new shoe</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input
                                    value={manufacturer}
                                    onChange={(e) => setManufacturer(e.target.value)}
                                    placeholder="Manufacturer"
                                    required
                                    type="text"
                                    name="manufacturer"
                                    id="manufacturer"
                                    className="form-control"/>
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={model_name}
                                    onChange={(e) => setModelName(e.target.value)}
                                    placeholder="Model Name"
                                    required
                                    type="text"
                                    name="model_name"
                                    id="model_name"
                                    className="form-control"/>
                                <label htmlFor="model_name">Model Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    value={color}
                                    onChange={(e) => setColor(e.target.value)}
                                    placeholder="Color"
                                    required
                                    type="text"
                                    name="color"
                                    id="color"
                                    className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                <input
                    value={picture_url}
                    onChange={(e) => setPicture(e.target.value)}
                    placeholder="Picture URL"
                    required
                    type="url"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
                {picture_url && <img src={picture_url} alt="Shoe Preview" style={{ maxWidth: '100%', marginTop: '10px' }} />}
            </div>
                            <div className="form-floating mb-3">
                                <select
                                    value={bin}
                                    onChange={(e) => setBin(e.target.value)}
                                    required
                                    name="bin_id"
                                    id="bin_id"
                                    className="form-select">
                                    <option value="">Select a Bin</option>
                                    {bins.map(bin => (
                                        <option key={bin.id} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    ))}
                                </select>
                                <label htmlFor="bin_id">Bin Name</label>
                            </div>

                            <button className="btn btn-primary" type="submit">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ShoeForm;