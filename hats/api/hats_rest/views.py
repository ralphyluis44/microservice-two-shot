from django.shortcuts import render
from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties =[
        "import_href",
        "closet_name"
    ]

class HatListEncoder(ModelEncoder):
    model=Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model=Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def hats_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message":"invalid location "},
                status=400,
                )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False)

def hats_detail(request, pk):
    try:
        hat = Hat.objects.get(pk=pk)

        if request.method == 'GET':
            return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)

        elif request.method == 'PUT':
            data = json.loads(request.body)
            for field, value in data.items():
                setattr(hat, field, value)
            hat.save()
            return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)

        elif request.method == 'DELETE':
            hat.delete()
            return JsonResponse({'message': 'Hat deleted successfully.'}, status=200)

    except Hat.DoesNotExist:
        return JsonResponse({'error': 'Hat not found.'}, status=404)
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON.'}, status=400)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)