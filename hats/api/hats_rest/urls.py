from django.contrib import admin
from django.urls import path, include
from hats_rest.views import hats_list, hats_detail

urlpatterns = [
    path('hats/', hats_list, name='hats_list'),
    path('hats/<int:pk>/', hats_detail, name='hats_detail'),
]
