from django.shortcuts import render
from django.http import JsonResponse
from .models import Shoe
from django.views.decorators.http import require_http_methods
import json
from .models import BinVO, Shoe
from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        'closet_name',
        'bin_number',
        'bin_size',
        'import_href'
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def shoes_list(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": list(shoes)},
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid bin id"},
                    status=400,
                )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def shoes_detail(request, pk):
    try:
        shoe = Shoe.objects.get(pk=pk)

        if request.method == 'GET':
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

        elif request.method == 'PUT':
            data = json.loads(request.body)
            for field, value in data.items():
                setattr(shoe, field, value)
            shoe.save()
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

        elif request.method == 'DELETE':
            shoe.delete()
            return JsonResponse({'message': 'Shoe deleted successfully.'}, status=200)

    except Shoe.DoesNotExist:
        return JsonResponse({'error': 'Shoe not found.'}, status=404)
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON.'}, status=400)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=500)