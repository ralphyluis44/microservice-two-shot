# Wardrobify

Team:

* Stephen - Hats
* Ralphy  - Shoes

## Design
The color of this is going to be yellow and white. It is going to have a gradient background and it is going to have a preview in the pictures that are imported from the links. It will also be styled by cards instead of a list format so that it can appear like StockX's website instead of a table format.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

My items from shoe Model consist of manufacturer, model_name, color, picture_url, and the bin. In the Shoe MOdel it holds a ForeignKey where the BinVO which makes it easier for the shoes to be stored in the wardrobe api through it being acced by the import_href in the BINVO model.

## Hats microservice

-LocationVO` Model: this has the storage location with a name and a URL for integrating the wardrobe micro service. 
-Hat model - describes a hat by its attributes: fabric, style, color and includes a picture url. Each hat is linked to a LocationVO to show where it is stored
-import_href - allows for data import based on unique URLs